﻿using System;

namespace SubtitleTimeshift
{
    public interface ISubtitle<T>
    {
        string Id { get; }
        string Start { get; }
        string End { get; }
        string Text { get; }

    }
    public class Subtitle<T> : ISubtitle<T>
    {

        public string Id { get; private set; }
        public string Start { get; private set; }
        public string End { get; private set; }
        public string Text { get; private set; }

        public Subtitle(string id, string start, string end, string text)
        {
            Id = id;
            Start = start;
            End = end;
            Text = text;
        }

        public void Sinc(TimeSpan span)
        {
            Start = TimeSpan.Parse(Start.ToString()).Add(span).ToString(@"hh\:mm\:ss\.fff");
            End = TimeSpan.Parse(End.ToString()).Add(span).ToString(@"hh\:mm\:ss\.fff");
        }
        public override string ToString()
        {
            /*Id
             *Start --> End
             *Text
             * 
             */

            return $"{Id}{Environment.NewLine}{Start} --> {End}{Environment.NewLine}{Text}{Environment.NewLine}{Environment.NewLine}";

        }
    }
}