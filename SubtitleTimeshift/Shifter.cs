﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SubtitleTimeshift
{
    public class Shifter
    {
        public static void Shift(Stream input, Stream output, TimeSpan timeSpan, Encoding encoding, int bufferSize = 1024, bool leaveOpen = false)
        {

            using (var reader = new StreamReader(input, encoding))
            {
                using (var writer = new StreamWriter(output, encoding))
                {
                    string line;
                    List<Subtitle<string>> Lista = getSubtitles(reader);
                    foreach (var item in Lista)
                    {
                        item.Sinc(timeSpan);
                        writer.Write(item.ToString());
                    }
                }
            }
        }

        private static List<Subtitle<string>> getSubtitles(StreamReader reader)
        {
            List<Subtitle<string>> Lista = new List<Subtitle<string>>();

            string line;
            string id = "";
            string start = "";
            string end = "";
            string text = "";
            //00:00:49,203 --> 00:00:52,623
            while ((line = reader.ReadLine()) != null)
            {
                if (String.IsNullOrEmpty(id))
                {
                    id = line.ToString();
                }
                else if (String.IsNullOrEmpty(start))
                {
                    start = line.Substring(0, 12);
                    end = line.Substring(17);
                }
                else if (!String.IsNullOrEmpty(line))
                {
                    if (String.IsNullOrEmpty(text))
                    {
                        text = line.ToString();
                    }
                    else
                    {
                        text = text + Environment.NewLine + line;
                    }
                }
                else if (String.IsNullOrEmpty(line))
                {
                    Subtitle<string> Subtitulo = new Subtitle<string>(id, start, end, text);
                    id = "";
                    start = "";
                    end = "";
                    text = "";
                    Lista.Add(Subtitulo);
                }
            }

            return Lista;
        }
    }
}
