﻿using System.Linq;
using System.Collections.Generic;

namespace Graph
{
    public interface IGraph<T>
    {
        List<string> RoutesBetween(T source, T target);

    }

    public class Graph<T> : IGraph<T>
    {

        private IEnumerable<ILink<T>> _links;
        private List<string> _caminhos;

        public Graph(IEnumerable<ILink<T>> links)
        {
            _links = links;
        }

        public List<string> RoutesBetween(T source, T target)
        {
            _caminhos = new List<string>();
            Rotas(source, target, source.ToString());
            return _caminhos;
        }


        public void Rotas(T source, T target, string rotaAtual = "")
        {
            //Busca as rotas que saem do ponto
            var rotasPossiveis = _links.Where(x => x.Source.Equals(source) && !rotaAtual.Contains(x.Target.ToString())).ToList();

            //Para cada rota possivel
            foreach (var item in rotasPossiveis)
            {
                Rotas(item.Target, target, rotaAtual + "-" + item.Target.ToString());
            }
            //Chegou no destino
            if (rotaAtual.Substring(rotaAtual.Length - 1).Equals(target.ToString()))
            {
                _caminhos.Add(rotaAtual);
                rotaAtual = "";
            }

        }
    }
}
